package com.lotsof.repository.rating.productAntiRating;

import com.lotsof.entity.rating.ProductAntiRating;

import java.util.Calendar;

/**
 * Created by vlad on 3/16/16.
 */
public interface ProductAntiRatingRepository {
    void create(Long productId, Double antiRatingValue, Long userId, Calendar antiRatingDate);

    ProductAntiRating read(Long id);

    void delete(Long id);

    void deleteProductAntiRating(Long productId);

    Double getAntiRatingValue(Long id);

    Double getSumOfAntiRatingValues(Long productId);

}
