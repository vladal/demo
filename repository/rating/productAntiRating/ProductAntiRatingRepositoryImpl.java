package com.lotsof.repository.rating.productAntiRating;

import com.lotsof.entity.rating.ProductAntiRating;
import com.lotsof.repository.CommonRepository;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Calendar;

/**
 * Created by vlad on 3/16/16.
 */
@Repository
public class ProductAntiRatingRepositoryImpl implements ProductAntiRatingRepository {
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public void create(Long productId, Double antiRatingValue, Long userId, Calendar antiRatingDate) {
        commonRepository.getSession()
                .createSQLQuery("INSERT INTO product_anti_rating (id, product_id, anti_rating_value, user_id, anti_rating_date) " +
                        "VALUES (nextval('product_anti_ratings_seq'), :productId, :antiRatingValue, :userId, :antiRatingDate)")
                .setParameter("productId", productId)
                .setParameter("antiRatingValue", antiRatingValue)
                .setParameter("userId", userId)
                .setParameter("antiRatingDate", antiRatingDate)
                .executeUpdate();
    }

    @Override
    public ProductAntiRating read(Long id) {
        return (ProductAntiRating) commonRepository.getSession()
                .createCriteria(ProductAntiRating.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    public void delete(Long id) {
        commonRepository.getSession()
                .createQuery("delete from ProductAntiRating where id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void deleteProductAntiRating(Long productId) {
        commonRepository.getSession()
                .createQuery("delete from ProductAntiRating where productId = :productId")
                .setParameter("productId", productId)
                .executeUpdate();
    }

    @Override
    public Double getAntiRatingValue(Long id) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductAntiRating.class)
                .add(Restrictions.eq("id", id))
                .setProjection(Projections.property("antiRatingValue"))
                .uniqueResult();
    }

    @Override
    public Double getSumOfAntiRatingValues(Long productId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductAntiRating.class)
                .add(Restrictions.eq("productId", productId))
                .setProjection(Projections.sum("antiRatingValue"))
                .uniqueResult();
    }
}
