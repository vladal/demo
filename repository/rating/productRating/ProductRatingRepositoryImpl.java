package com.lotsof.repository.rating.productRating;

import com.lotsof.entity.rating.ProductRating;
import com.lotsof.repository.CommonRepository;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vlad.
 */
@Repository
public class ProductRatingRepositoryImpl implements ProductRatingRepository {
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public void create(Long productId, Long countryId, Double rating) {
        commonRepository.getSession()
                .createSQLQuery("INSERT INTO product_rating (id, product_id, country_id, rating) " +
                        "VALUES (nextval('product_instant_ratings_seq'), :productId, :countryId, :rating)")
                .setParameter("productId", productId)
                .setParameter("countryId", countryId)
                .setParameter("rating", rating)
                .executeUpdate();
    }

    @Override
    public int update(Long productId, Long countryId, Double rating) {
        return commonRepository.getSession()
                .createQuery("UPDATE ProductRating productRating " +
                        "SET productRating.rating = :rating " +
                        "WHERE productRating.productId = :productId AND productRating.countryId = :countryId")
                .setParameter("productId", productId)
                .setParameter("countryId", countryId)
                .setParameter("rating", rating)
                .executeUpdate();
    }

    @Override
    public Double getProductRatingByCountry(Long productId, Long countryId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("countryId", countryId)))
                .setProjection(Projections.property("rating"))
                .uniqueResult();
    }

    @Override
    public Map<Long, Double> getProductRatingsByCountryList(Long productId, List<Long> countryIdList) {
        Map<Long, Double> resultMap = new HashMap<>(countryIdList.size());
        for (Long countryId : countryIdList) {
            resultMap.put(countryId, 1.0);
        }
        List result = commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.in("countryId", countryIdList)))
                .setProjection(Projections.projectionList()
                        .add(Projections.property("countryId"))
                        .add(Projections.property("rating")))
                .setResultTransformer(Transformers.TO_LIST)
                .list();
        for (Object resultElement : result) {
            List temp = (List) resultElement;
            resultMap.put((Long) temp.get(0), (Double) temp.get(1));
        }
        return resultMap;
    }

    @Override
    public Double getMaxProductRatingByCountry(Long countryId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.eq("countryId", countryId))
                .setProjection(Projections.max("rating"))
                .uniqueResult();
    }

    @Override
    public Long getProductIdWithMaxRatingByCountry(Long countryId) {
        return (Long) commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.eq("countryId", countryId))
                .addOrder(Order.desc("rating"))
                .setProjection(Projections.property("productId"))
                .setFirstResult(0)
                .setMaxResults(1)
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getProductIdListSortedByRating(Long countryId) {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.eq("countryId", countryId))
                .addOrder(Order.desc("rating"))
                .setProjection(Projections.property("productId"))
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getProductIdListSortedByRating(Long countryId, int firstResult, int maxResults) {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.eq("countryId", countryId))
                .addOrder(Order.desc("rating"))
                .setProjection(Projections.property("productId"))
                .setFirstResult(firstResult)
                .setMaxResults(maxResults)
                .list();
    }

    @Override
    public Double getProductRating(Long productId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.eq("productId", productId))
                .setProjection(Projections.avg("rating"))
                .uniqueResult();
    }

    @Override
    public Double getAverageRating(Long countryId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductRating.class)
                .add(Restrictions.eq("countryId", countryId))
                .setProjection(Projections.avg("rating"))
                .uniqueResult();
    }
}
