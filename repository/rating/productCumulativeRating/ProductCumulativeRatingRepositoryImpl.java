package com.lotsof.repository.rating.productCumulativeRating;

import com.lotsof.entity.rating.ProductCumulativeRating;
import com.lotsof.repository.CommonRepository;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vlad.
 */
@Repository
public class ProductCumulativeRatingRepositoryImpl implements ProductCumulativeRatingRepository {
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public void create(Long productId, double rating) {
        commonRepository.getSession()
                .createSQLQuery("INSERT INTO product_cumulative_rating (id, product_id, rating) " +
                        "VALUES (nextval('product_cumulative_ratings_seq'), :productId, :rating)")
                .setParameter("productId", productId)
                .setParameter("rating", rating)
                .executeUpdate();
    }

    @Override
    public int update(Long productId, double rating) {
        return commonRepository.getSession()
                .createQuery("UPDATE ProductCumulativeRating cumulativeRating " +
                        "SET cumulativeRating.rating = :rating " +
                        "WHERE cumulativeRating.productId = :productId")
                .setParameter("rating", rating)
                .setParameter("productId", productId)
                .executeUpdate();
    }

    @Override
    public Double getRating(Long productId) {
        Double result = (Double) commonRepository.getSession()
                .createCriteria(ProductCumulativeRating.class)
                .add(Restrictions.eq("productId", productId))
                .setProjection(Projections.property("rating"))
                .uniqueResult();
        if (result == null) {
            result = 0.0;
        }
        return result;
    }

    @Override
    public Double getMaxRating() {
        Double result = (Double) commonRepository.getSession()
                .createCriteria(ProductCumulativeRating.class)
                .setProjection(Projections.max("rating"))
                .uniqueResult();
        if (result == null) {
            result = 0.0;
        }
        return result;
    }

    @Override
    public Double getAverageRating() {
        Double result = (Double) commonRepository.getSession()
                .createCriteria(ProductCumulativeRating.class)
                .setProjection(Projections.avg("rating"))
                .uniqueResult();
        if (result == null) {
            result = 0.0;
        }
        return result;
    }

    @Override
    public Long getProductIdWithMaxRating() {
        return (Long) commonRepository.getSession()
                .createCriteria(ProductCumulativeRating.class)
                .addOrder(Order.desc("rating"))
                .setProjection(Projections.property("productId"))
                .setFirstResult(0)
                .setMaxResults(1)
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getProductIdListSortedByRating(int firstResult, int maxResults) {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductCumulativeRating.class)
                .addOrder(Order.desc("rating"))
                .setProjection(Projections.property("productId"))
                .setFirstResult(firstResult)
                .setMaxResults(maxResults)
                .list();
    }
}
