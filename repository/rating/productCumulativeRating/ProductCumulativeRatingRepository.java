package com.lotsof.repository.rating.productCumulativeRating;

import java.util.List;

/**
 * Created by vlad.
 */
public interface ProductCumulativeRatingRepository {
    void create(Long productId, double rating);

    int update(Long productId, double rating);

    Double getRating(Long productId);

    Double getMaxRating();

    Double getAverageRating();

    Long getProductIdWithMaxRating();

    List<Long> getProductIdListSortedByRating(int firstResult, int maxResults);
}
