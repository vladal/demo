package com.lotsof.repository.rating.cumulativeData;

import com.lotsof.common.rating.UserEventType;
import com.lotsof.entity.rating.ProductRatingCumulativeData;
import com.lotsof.repository.CommonRepository;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlad.
 */
@Repository
public class
        ProductRatingCumulativeDataRepositoryImpl implements ProductRatingCumulativeDataRepository {
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public void create(Long productId, UserEventType eventType, Long countryId) {
        commonRepository.getSession()
                .createSQLQuery("INSERT INTO product_rating_cumulative_data (id, product_id, event_type, country_id, counter) " +
                        "VALUES (nextval('product_rating_cumulative_datas_seq'), :productId, :eventType, :countryId, :counter)")
                .setParameter("productId", productId)
                .setParameter("eventType", eventType.getEvent())
                .setParameter("countryId", countryId)
                .setParameter("counter", 1L)
                .executeUpdate();
    }

    @Override
    public int update(Long productId, UserEventType eventType, Long countryId) {
        return commonRepository.getSession()
                .createQuery("UPDATE ProductRatingCumulativeData cumulativeData " +
                        "SET cumulativeData.counter=cumulativeData.counter + 1 " +
                        "WHERE cumulativeData.productId = :productId AND cumulativeData.eventType = :eventType AND " +
                        "cumulativeData.countryId = :countryId")
                .setParameter("productId", productId)
                .setParameter("eventType", eventType.getEvent())
                .setParameter("countryId", countryId)
                .executeUpdate();
    }

    @Override
    public Long getCounterValue(Long productId, UserEventType eventType, Long countryId) {
        return (Long) commonRepository.getSession()
                .createCriteria(ProductRatingCumulativeData.class)
                .add(Restrictions.and(
                        Restrictions.and(
                                Restrictions.eq("productId", productId),
                                Restrictions.eq("countryId", countryId)),
                        Restrictions.eq("eventType", eventType.getEvent())))
                .setProjection(Projections.property("counter"))
                .uniqueResult();
    }

    @Override
    public Long getCounterValue(Long productId, UserEventType eventType) {
        return (Long) commonRepository.getSession()
                .createCriteria(ProductRatingCumulativeData.class)
                .add(Restrictions.eq("productId", productId))
                .add(Restrictions.eq("eventType", eventType.getEvent()))
                .setProjection(Projections.sum("counter"))
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getProductIds() {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductRatingCumulativeData.class)
                .setProjection(Projections.distinct(Projections.property("productId")))
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getCountries(Long productId) {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductRatingCumulativeData.class)
                .add(Restrictions.eq("productId", productId))
                .setProjection(Projections.distinct(Projections.property("countryId")))
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getAllCountries() {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductRatingCumulativeData.class)
                .setProjection(Projections.distinct(Projections.property("countryId")))
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductRatingCumulativeData> getListProductRatingCumulativeData(Long productId, Long countryId) {
        return (List<ProductRatingCumulativeData>) commonRepository.getSession()
                .createCriteria(ProductRatingCumulativeData.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("countryId", countryId)))
                .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Long> getProductIdsWithMaxSells(List<Long> productIds, UserEventType eventType, int maxCount) {
        if (productIds == null || productIds.isEmpty()) {
            return new ArrayList<>();
        }
        return commonRepository.getSession().createCriteria(ProductRatingCumulativeData.class)
                .setFirstResult(0)
                .setMaxResults(maxCount)
                .addOrder(Order.desc("counter"))
                .add(Restrictions.and(
                                Restrictions.in("productId", productIds),
                                Restrictions.eq("eventType", eventType.getEvent())
                        )
                )
                .list();
    }
}
