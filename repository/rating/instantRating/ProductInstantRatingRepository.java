package com.lotsof.repository.rating.instantRating;

import com.lotsof.entity.rating.ProductInstantRating;

import java.util.List;

/**
 * Created by vlad.
 */
public interface ProductInstantRatingRepository {
    void create(Long productId, Long countryId, double instantRating);

    List<ProductInstantRating> getLastInstantRatings(Long productId, Long countryId, int countOfLastResults);

    Double movingAverage(Long productId, Long countryId, int countOfLastResults);
}
