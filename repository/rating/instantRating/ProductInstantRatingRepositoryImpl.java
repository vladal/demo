package com.lotsof.repository.rating.instantRating;

import com.lotsof.entity.rating.ProductInstantRating;
import com.lotsof.repository.CommonRepository;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.List;

/**
 * Created by vlad.
 */
@Repository
public class ProductInstantRatingRepositoryImpl implements ProductInstantRatingRepository {
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public void create(Long productId, Long countryId, double instantRating) {
        commonRepository.getSession()
                .createSQLQuery("INSERT INTO product_instant_rating (id, datetime, product_id, country_id, instant_rating) " +
                        "VALUES (nextval('product_instant_ratings_seq'), :datetime, :productId, :countryId, :instantRating)")
                .setParameter("datetime", Calendar.getInstance())
                .setParameter("productId", productId)
                .setParameter("countryId", countryId)
                .setParameter("instantRating", instantRating)
                .executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductInstantRating> getLastInstantRatings(Long productId, Long countryId, int countOfLastResults) {
        return commonRepository.getSession()
                .createCriteria(ProductInstantRating.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("countryId", countryId)))
                .addOrder(Order.desc("id"))
                .setFirstResult(0)
                .setMaxResults(countOfLastResults)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Double movingAverage(Long productId, Long countryId, int countOfLastResults) {
        Double result = 0.0;
        List<Double> instantRatings = commonRepository.getSession()
                .createCriteria(ProductInstantRating.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("countryId", countryId)))
                .addOrder(Order.desc("id"))
                .setFirstResult(0)
                .setMaxResults(countOfLastResults)
                .setProjection(Projections.property("instantRating"))
                .list();
        if (instantRatings != null && !instantRatings.isEmpty()) {
            for (Double instantRating : instantRatings) {
                result += instantRating;
            }
            result /= instantRatings.size();
        }
        return result;
    }
}
