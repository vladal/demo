package com.lotsof.repository.rating.productRatingBoost;

import com.lotsof.entity.rating.ProductRatingBoost;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by vlad.
 */
public interface ProductRatingBoostRepository {

    int create(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId, Calendar boostDate);

    int create(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId);

    ProductRatingBoost create(ProductRatingBoost productRatingBoost);

    ProductRatingBoost read(Long id);

    ProductRatingBoost getByProductId(Long productId, Long boostCountryId, Long userId);

    List<ProductRatingBoost> getListByProductId(Long productId, Long boostCountryId);

    Map<Long, List<ProductRatingBoost>> getBoostMapByProductId(Long productId, List<Long> boostCountryIdList);

//    int update(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Calendar boostDate);
//
//    int update(Long productId, Double boostFactor, Double boostValue, Long boostCountryId);
//
//    int update(ProductRatingBoost productRatingBoost);

    void remove(ProductRatingBoost productRatingBoost);

    void remove(Long id);

    void remove(Long productId, Long boostCountryId, Long userId);

    void removeProductRatingBoost(Long productId, Long boostCountryId, Long userId);

    Double getBoostFactor(Long productId, Long boostCountryId, Long userId);

    Double getSumBoostValue(Long productId, Long boostCountryId);

    Double getRatingBoostValue(Long productId, Long boostCountryId, Long userId);

    Calendar getBoostDate(Long productId, Long boostCountryId, Long userId);

    Calendar getBoostToDate(Long productId, Long boostCountryId, Long userId);

    List<Long> getProductIdList(int firstResult, int maxResults);

    List<Long> getActiveProductIdList(int firstResult, int maxResults);

    List<ProductRatingBoost> getProductRatingBoostList(int firstResult, int maxResults);

    void resetProductRatingBoost(Long productId);

//    List<ProductRatingBoost> getActiveProductRatingBoostList(int firstResult, int maxResults);

}
