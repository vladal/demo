package com.lotsof.repository.rating.productRatingBoost;

import com.lotsof.entity.rating.ProductRatingBoost;
import com.lotsof.repository.CommonRepository;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by vlad.
 */
@Repository
public class ProductRatingBoostRepositoryImpl implements ProductRatingBoostRepository {

    private static final int DAYS_TO_BOOST = 30;

    @Autowired
    private CommonRepository commonRepository;

    @Override
    public int create(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId, Calendar boostDate) {
        Calendar boostToDate = getBoostToDate(boostDate);
        return commonRepository.getSession()
                .createSQLQuery("INSERT INTO product_rating_boost (id, " +
                        "product_id, " +
                        "boost_factor, " +
                        "boost_value, " +
                        "boost_country_id, " +
                        "user_id, " +
                        "boost_date, " +
                        "boost_to_date) " +
                        "VALUES (nextval('product_rating_boosts_seq'), :productId, " +
                        ":boostFactor, :boostValue, :boostCountryId, :userId, :boostDate, :boostToDate)")
                .setParameter("productId", productId)
                .setParameter("boostFactor", boostFactor)
                .setParameter("boostValue", boostValue)
                .setParameter("boostCountryId", boostCountryId)
                .setParameter("userId", userId)
                .setParameter("boostDate", boostDate)
                .setParameter("boostToDate", boostToDate)
                .executeUpdate();
    }

    @Override
    public int create(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId) {
        return create(productId, boostFactor, boostValue, boostCountryId, userId, Calendar.getInstance());
    }

    @Override
    public ProductRatingBoost create(ProductRatingBoost productRatingBoost) {
        commonRepository.create(productRatingBoost);
        return productRatingBoost;
    }

    @Override
    public ProductRatingBoost read(Long id) {
        return commonRepository.read(ProductRatingBoost.class, id);
    }

    @Override
    public ProductRatingBoost getByProductId(Long productId, Long boostCountryId, Long userId) {
        return (ProductRatingBoost) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("boostCountryId", boostCountryId),
                        Restrictions.eq("userId", userId)
                ))
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductRatingBoost> getListByProductId(Long productId, Long boostCountryId) {
        return (List<ProductRatingBoost>) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("boostCountryId", boostCountryId)
                ))
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<Long, List<ProductRatingBoost>> getBoostMapByProductId(Long productId, List<Long> boostCountryIdList) {
        List<ProductRatingBoost> resultList = (List<ProductRatingBoost>) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.in("boostCountryId", boostCountryIdList)
                        )
                )
                .list();
        Map<Long, List<ProductRatingBoost>> resultMap = new HashMap<>(resultList.size());
        for (ProductRatingBoost productRatingBoost : resultList) {
            if (!resultMap.containsKey(productRatingBoost.getBoostCountryId())) {
                resultMap.put(productRatingBoost.getBoostCountryId(), new ArrayList<>());
            }
            resultMap.get(productRatingBoost.getBoostCountryId()).add(productRatingBoost);
        }
        return resultMap;
    }

//    @Override
//    public int update(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Calendar boostDate) {
//        Calendar boostToDate = getBoostToDate(boostDate);
//        return commonRepository.getSession()
//                .createQuery("UPDATE ProductRatingBoost productRatingBoost " +
//                        "SET productRatingBoost.boostFactor = :boostFactor, " +
//                        "productRatingBoost.boostValue = :boostValue, " +
//                        "productRatingBoost.boostDate = :boostDate, " +
//                        "productRatingBoost.boostToDate = :boostToDate " +
//                        "WHERE productRatingBoost.productId = :productId AND " +
//                        "productRatingBoost.boostCountryId = :boostCountryId")
//                .setParameter("boostFactor", boostFactor)
//                .setParameter("boostValue", boostValue)
//                .setParameter("boostCountryId", boostCountryId)
//                .setParameter("boostDate", boostDate)
//                .setParameter("boostToDate", boostToDate)
//                .setParameter("productId", productId)
//                .executeUpdate();
//    }
//
//    @Override
//    public int update(Long productId, Double boostFactor, Double boostValue, Long boostCountryId) {
//        if (boostFactor != null && boostFactor > 0.0) {
//            return update(productId, boostFactor, boostValue, boostCountryId, Calendar.getInstance());
//        } else {
//            return update(productId, 0.0, 0.0, boostCountryId, null);
//        }
//    }
//
//    @Override
//    public int update(ProductRatingBoost productRatingBoost) {
//        commonRepository.getSession().update(productRatingBoost);
//        if (productRatingBoost.getId() == null || productRatingBoost.getId() == 0L) {
//            return 0;
//        } else {
//            return 1;
//        }
//    }

    @Override
    public void remove(ProductRatingBoost productRatingBoost) {
        commonRepository.delete(productRatingBoost);
    }

    @Override
    public void remove(Long id) {
        commonRepository.getSession()
                .createQuery("DELETE FROM ProductRatingBoost WHERE id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void remove(Long productId, Long boostCountryId, Long userId) {
        commonRepository.getSession()
                .createQuery("DELETE FROM ProductRatingBoost WHERE productId = :productId AND " +
                        "boostCountryId = :boostCountryId AND userId = :userId")
                .setParameter("productId", productId)
                .setParameter("boostCountryId", boostCountryId)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeProductRatingBoost(Long productId, Long boostCountryId, Long userId) {
        commonRepository.getSession()
                .createQuery("DELETE FROM ProductRatingBoost WHERE productId = :productId AND " +
                        "boostCountryId = :boostCountryId AND " +
                        "userId = :userId")
                .setParameter("productId", productId)
                .setParameter("boostCountryId", boostCountryId)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public Double getBoostFactor(Long productId, Long boostCountryId, Long userId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("boostCountryId", boostCountryId),
                        Restrictions.eq("userId", userId)
                ))
                .setProjection(Projections.property("boostFactor"))
                .uniqueResult();
    }

    @Override
    public Double getSumBoostValue(Long productId, Long boostCountryId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("boostCountryId", boostCountryId)
                ))
                .setProjection(Projections.sum("boostValue"))
                .uniqueResult();
    }

    @Override
    public Double getRatingBoostValue(Long productId, Long boostCountryId, Long userId) {
        return (Double) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("boostCountryId", boostCountryId),
                        Restrictions.eq("userId", userId)
                ))
                .setProjection(Projections.property("boostValue"))
                .uniqueResult();
    }

    @Override
    public Calendar getBoostDate(Long productId, Long boostCountryId, Long userId) {
        return (Calendar) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("boostCountryId", boostCountryId),
                        Restrictions.eq("userId", userId)
                ))
                .setProjection(Projections.property("boostDate"))
                .uniqueResult();
    }

    @Override
    public Calendar getBoostToDate(Long productId, Long boostCountryId, Long userId) {
        return (Calendar) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.eq("productId", productId),
                        Restrictions.eq("boostCountryId", boostCountryId),
                        Restrictions.eq("userId", userId)
                ))
                .setProjection(Projections.property("boostToDate"))
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getProductIdList(int firstResult, int maxResults) {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .setProjection(Projections.property("productId"))
                .setFirstResult(firstResult)
                .setMaxResults(maxResults)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getActiveProductIdList(int firstResult, int maxResults) {
        return (List<Long>) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .add(Restrictions.and(
                        Restrictions.isNotNull("boostDate"),
                        Restrictions.isNotNull("boostToDate"),
                        Restrictions.neOrIsNotNull("boostFactor", 0.0),
                        Restrictions.neOrIsNotNull("boostValue", 0.0)
                ))
                .setProjection(Projections.property("productId"))
                .setFirstResult(firstResult)
                .setMaxResults(maxResults)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductRatingBoost> getProductRatingBoostList(int firstResult, int maxResults) {
        return (List<ProductRatingBoost>) commonRepository.getSession()
                .createCriteria(ProductRatingBoost.class)
                .setFirstResult(firstResult)
                .setMaxResults(maxResults)
                .list();
    }

    @Override
    public void resetProductRatingBoost(Long productId) {
        commonRepository.getSession()
                .createQuery("delete from ProductRatingBoost where productId= :productId")
                .setParameter("productId", productId)
                .executeUpdate();
    }

//    @SuppressWarnings("unchecked")
//    @Override
//    public List<ProductRatingBoost> getActiveProductRatingBoostList(int firstResult, int maxResults) {
//        return (List<ProductRatingBoost>) commonRepository.getSession()
//                .createCriteria(ProductRatingBoost.class)
//                .add(Restrictions.and(
//                        Restrictions.isNotNull("boostDate"),
//                        Restrictions.isNotNull("boostToDate"),
//                        Restrictions.neOrIsNotNull("boostFactor", 0.0),
//                        Restrictions.neOrIsNotNull("boostValue", 0.0)
//                ))
//                .setFirstResult(firstResult)
//                .setMaxResults(maxResults)
//                .list();
//    }

    private Calendar getBoostToDate(Calendar boostDate) {
        Calendar boostToDate = null;
        if (boostDate != null) {
            boostToDate = (Calendar) boostDate.clone();
            boostToDate.add(Calendar.DATE, DAYS_TO_BOOST);
        }
        return boostToDate;
    }

    private Calendar getBoostToDate(Calendar boostDate, final int daysToBoost) {
        Calendar boostToDate = null;
        if (boostDate != null && daysToBoost > 0) {
            boostToDate = (Calendar) boostDate.clone();
            boostToDate.add(Calendar.DATE, daysToBoost);
        }
        return boostToDate;
    }
}
