package com.lotsof.repository.rating.rawData;

import com.lotsof.common.rating.UserEventType;

/**
 * Created by vlad.
 */
public interface ProductRatingRawDataRepository {
    void create(Long userId, Long countryId, UserEventType eventType, Long productId, Long shopId);
}
