package com.lotsof.repository.rating.rawData;

import com.lotsof.common.rating.UserEventType;
import com.lotsof.repository.CommonRepository;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Calendar;

/**
 * Created by vlad.
 */
@Repository
public class ProductRatingRawDataRepositoryImpl implements ProductRatingRawDataRepository {
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public void create(Long userId, Long countryId, UserEventType eventType, Long productId, Long shopId) {
        commonRepository.getSession()
                .createSQLQuery("INSERT INTO product_rating_raw_data (id, datetime, user_id, country_id, event_type, product_id, shop_id) " +
                        "VALUES (nextval('product_rating_raw_datas_seq'), :datetime, :userId, :countryId, :eventType, :productId, :shopId)")
                .setParameter("datetime", Calendar.getInstance())
                .setParameter("userId", userId, new LongType())
                .setParameter("countryId", countryId, new LongType())
                .setParameter("eventType", eventType.getEvent())
                .setParameter("productId", productId)
                .setParameter("shopId", shopId)
                .executeUpdate();
    }
}
