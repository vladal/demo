package com.lotsof.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by vlad on 1/5/16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseRequest implements Serializable {

    private String redirectTo;

    public BaseRequest() {
    }

    public BaseRequest(String redirectTo) {
        this.redirectTo = redirectTo;
    }

    public String getRedirectTo() {
        return redirectTo;
    }

    public void setRedirectTo(String redirectTo) {
        this.redirectTo = redirectTo;
    }
}
