package com.lotsof.api;

import org.springframework.validation.ObjectError;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlad.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseResponse implements Serializable {

    String messageId;

    private List<ErrorPair> errors;

    public BaseResponse() {
    }

    public BaseResponse(String messageId) {
        this.messageId = messageId;
    }

    public BaseResponse(String key, String errMsg) {
        addError(key, errMsg);
    }

    public BaseResponse(List<ObjectError> bindingErrors) {
        if (bindingErrors != null && bindingErrors.size() > 0) {
            for (ObjectError bindingError : bindingErrors) {
                addError(bindingError.getCode(), bindingError.getDefaultMessage());
            }
        }
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public List<ErrorPair> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorPair> errors) {
        this.errors = errors;
    }

    public void addError(String errorKey, String error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        errors.add(new ErrorPair(errorKey, error));
    }

    public void addErrors(List<ObjectError> objectErrors) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        if (objectErrors != null) {
            for (ObjectError objectError : objectErrors) {
                errors.add(new ErrorPair(objectError.getCode(), objectError.getDefaultMessage()));
            }
        }
    }
}
