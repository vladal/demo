package com.lotsof.api.ratingBoost;

import com.lotsof.api.BaseResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by vlad.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductBoostResponse extends BaseResponse implements Serializable {
    private boolean success;

    public ProductBoostResponse() {
    }

    public ProductBoostResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
