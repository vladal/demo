package com.lotsof.api.rating;

import com.lotsof.api.BaseResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by vlad on 3/16/16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductRatingResponse extends BaseResponse {
    private boolean success;
    private Double productRating;
    private Double productRatingWithBoost;
    private Double productRatingWithCore;

    public ProductRatingResponse() {
    }

    public ProductRatingResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Double getProductRating() {
        return productRating;
    }

    public void setProductRating(Double productRating) {
        this.productRating = productRating;
    }

    public Double getProductRatingWithBoost() {
        return productRatingWithBoost;
    }

    public void setProductRatingWithBoost(Double productRatingWithBoost) {
        this.productRatingWithBoost = productRatingWithBoost;
    }

    public Double getProductRatingWithCore() {
        return productRatingWithCore;
    }

    public void setProductRatingWithCore(Double productRatingWithCore) {
        this.productRatingWithCore = productRatingWithCore;
    }
}
