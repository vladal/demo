package com.lotsof.api.rating;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by vlad on 3/16/16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductResetRatingRequest implements Serializable {
    private Long productId;

    public ProductResetRatingRequest() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
