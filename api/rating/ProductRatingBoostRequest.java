package com.lotsof.api.rating;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by sasha on 02.12.15.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductRatingBoostRequest implements Serializable {
    private Long productId;
    private Double boostFactor;

    public ProductRatingBoostRequest() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getBoostFactor() {
        return boostFactor;
    }

    public void setBoostFactor(Double boostFactor) {
        this.boostFactor = boostFactor;
    }
}
