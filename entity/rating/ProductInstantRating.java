package com.lotsof.entity.rating;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by vlad.
 */
@Entity
@Table(name = "product_instant_rating",
        indexes = {
                @Index(columnList = "product_id", name = "ProductInstantRating_product_id_idx"),
                @Index(columnList = "country_id", name = "ProductInstantRating_country_id_idx")
        })
public class ProductInstantRating implements Serializable {
    @Id
    @SequenceGenerator(name = "product_instant_rating_seq", sequenceName = "product_instant_ratings_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_instant_rating_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "datetime")
    @NotNull
    private Calendar time;

    @Column(name = "product_id")
    @NotNull
    private Long productId;

    @Column(name = "country_id")
    private Long countryId;

    @Column(name = "instant_rating")
    private Double instantRating;

    public ProductInstantRating() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getTime() {
        return time;
    }

    public void setTime(Calendar time) {
        this.time = time;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Double getInstantRating() {
        return instantRating;
    }

    public void setInstantRating(Double instantRating) {
        this.instantRating = instantRating;
    }
}
