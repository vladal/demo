package com.lotsof.entity.rating;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by vlad.
 */
@Entity
@Table(name = "product_cumulative_rating")
public class ProductCumulativeRating {
    @Id
    @SequenceGenerator(name = "product_cumulative_rating_seq", sequenceName = "product_cumulative_ratings_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_cumulative_rating_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "product_id")
    @NotNull
    private Long productId;

    @Column(name = "rating")
    private Double rating;

    public ProductCumulativeRating() {
    }

    public Long getId() {
        return id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}
