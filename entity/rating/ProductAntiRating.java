package com.lotsof.entity.rating;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by vlad on 3/16/16.
 */
@Entity
@Table(name = "product_anti_rating")
public class ProductAntiRating implements Serializable {
    @Id
    @SequenceGenerator(name = "product_anti_rating_seq", sequenceName = "product_anti_ratings_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_anti_rating_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "product_id", nullable = false)
    private Long productId;

    @Column(name = "anti_rating_value")
    private Double antiRatingValue;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "anti_rating_date", nullable = false)
    private Calendar antiRatingDate;

    public ProductAntiRating() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getAntiRatingValue() {
        return antiRatingValue;
    }

    public void setAntiRatingValue(Double antiRatingValue) {
        this.antiRatingValue = antiRatingValue;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Calendar getAntiRatingDate() {
        return antiRatingDate;
    }

    public void setAntiRatingDate(Calendar antiRatingDate) {
        this.antiRatingDate = antiRatingDate;
    }
}
