package com.lotsof.entity.rating;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by vlad.
 */
@Entity
@Table(name = "product_rating_cumulative_data",
        uniqueConstraints = @UniqueConstraint(columnNames = {"product_id","event_type","country_id"}))
public class ProductRatingCumulativeData implements Serializable {
    @Id
    @SequenceGenerator(name = "product_rating_cumulative_data_seq", sequenceName = "product_rating_cumulative_datas_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_rating_cumulative_data_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "product_id")
    @NotNull
    private Long productId;

    @Column(name = "event_type")
    @NotNull
    private String eventType;

    @Column(name = "country_id")
    @NotNull
    private Long countryId;

    @Column(name = "counter")
    private Long counter;

    public ProductRatingCumulativeData() {
    }

    public Long getId() {
        return id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }
}
