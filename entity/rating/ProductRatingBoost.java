package com.lotsof.entity.rating;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by vlad.
 */
@Entity
@Table(name = "product_rating_boost")
public class ProductRatingBoost implements Serializable {
    @Id
    @SequenceGenerator(name = "product_rating_boost_seq", sequenceName = "product_rating_boosts_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_rating_boost_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "boost_factor")
    private Double boostFactor;

    @Column(name = "boost_value")
    private Double boostValue;

    @Column(name = "boost_date")
    private Calendar boostDate;

    @Column(name = "boost_to_date")
    private Calendar boostToDate;

    @Column(name = "boost_country_id")
    private Long boostCountryId;

    @Column(name = "user_id")
    private Long userId;

    public ProductRatingBoost() {
    }

    public Long getId() {
        return id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getBoostFactor() {
        return boostFactor;
    }

    public void setBoostFactor(Double boostFactor) {
        this.boostFactor = boostFactor;
    }

    public Double getBoostValue() {
        return boostValue;
    }

    public void setBoostValue(Double boostValue) {
        this.boostValue = boostValue;
    }

    public Calendar getBoostDate() {
        return boostDate;
    }

    public void setBoostDate(Calendar boostDate) {
        this.boostDate = boostDate;
    }

    public Calendar getBoostToDate() {
        return boostToDate;
    }

    public void setBoostToDate(Calendar boostToDate) {
        this.boostToDate = boostToDate;
    }

    public Long getBoostCountryId() {
        return boostCountryId;
    }

    public void setBoostCountryId(Long boostCountryId) {
        this.boostCountryId = boostCountryId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
