package com.lotsof.entity.rating;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by vlad.
 */
@Entity
@Table(name = "product_rating_raw_data")
public class ProductRatingRawData implements Serializable {

    @Id
    @SequenceGenerator(name = "product_rating_raw_data_seq", sequenceName = "product_rating_raw_datas_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_rating_raw_data_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "event_type")
    @NotNull
    private String eventType;

    @Column(name = "product_id")
    @NotNull
    private Long productId;

    @Column(name = "datetime")
    @NotNull
    private Calendar dateTime;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "country_id")
    private Long countryId;

    @Column(name = "shop_id")
    @NotNull
    private Long shopId;

    public ProductRatingRawData() {
    }

    public Calendar getDateTime() {
        return dateTime;
    }

    public void setDateTime(Calendar dateTime) {
        this.dateTime = dateTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getId() {
        return id;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
