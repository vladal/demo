package com.lotsof.entity.rating;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by vlad.
 */
@Entity
@Table(name = "product_rating")
public class ProductRating implements Serializable {
    @Id
    @SequenceGenerator(name = "product_rating_seq", sequenceName = "product_ratings_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_rating_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "product_id")
    @NotNull
    private Long productId;

    @Column(name = "country_id")
    private Long countryId;

    @Column(name = "rating")
    private Double rating;

    public ProductRating() {
    }

    public Long getId() {
        return id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}
