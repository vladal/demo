package com.lotsof.services.rating.rawData;

import com.lotsof.common.rating.UserEventType;

/**
 * Created by vlad.
 */
public interface ProductRatingRawDataService {

    void create(Long userId, Long countryId, UserEventType eventType, Long productId, Long shopId);
}
