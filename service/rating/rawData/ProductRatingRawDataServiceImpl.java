package com.lotsof.services.rating.rawData;

import com.lotsof.common.rating.UserEventType;
import com.lotsof.repository.rating.rawData.ProductRatingRawDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by vlad.
 */
@Service
public class ProductRatingRawDataServiceImpl implements ProductRatingRawDataService {
    @Autowired
    private ProductRatingRawDataRepository productRatingRawDataRepository;

    @Transactional
    @Override
    public void create(Long userId, Long countryId, UserEventType eventType, Long productId, Long shopId) {
        productRatingRawDataRepository.create(userId, countryId, eventType, productId, shopId);
    }
}
