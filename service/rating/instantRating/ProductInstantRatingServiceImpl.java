package com.lotsof.services.rating.instantRating;

import com.lotsof.common.rating.UserEventType;
import com.lotsof.entity.rating.ProductInstantRating;
import com.lotsof.entity.rating.ProductRatingCumulativeData;
import com.lotsof.repository.rating.instantRating.ProductInstantRatingRepository;
import com.lotsof.services.rating.cumulativeData.ProductRatingCumulativeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by vlad.
 */
@Service
public class ProductInstantRatingServiceImpl implements ProductInstantRatingService {

    @Autowired
    private ProductInstantRatingRepository productInstantRatingRepository;

    @Autowired
    private ProductRatingCumulativeDataService productRatingCumulativeDataService;

    @Transactional
    @Override
    public void create(Long productId, Long countryId, Double instantRating) {
        productInstantRatingRepository.create(productId, countryId, instantRating);
    }

    /**
     * @param productId - Id product in DB
     * @param countryId - Id dictionary item in DB
     * @param m         - positive constant
     * @param c         - average rating of all products on marketplace
     * @return - Products instant rating based on formula:
     * weighted rating (WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C
     * <p>
     * R = average for the product (mean)
     * v = number of positive events for the product
     * m = minimum positive events required to be rated (currently 400)
     * C = the mean rating across the whole marketplace
     * <p>
     * Algorithm based on weighted rating algorithm imdb & kinopoisk
     * @link http://www.imdb.com/help/show_leaf?votestopfaq&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2239792642&pf_rd_r=09H587JX6A8SMEYZ4ZJG&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_faq
     * @link http://www.kinopoisk.ru/top/#formula
     * @link https://en.wikipedia.org/wiki/Internet_Movie_Database#Film_rankings_.28IMDb_Top_250.29
     */
    @Override
    public Double calculateProductInstantRating(Long productId, Long countryId, final Double m, final Double c) {
        List<ProductRatingCumulativeData> dataList =
                productRatingCumulativeDataService.getListProductRatingCumulativeData(productId, countryId);

        Double result = 0.0;
        Long counter = 0L;
        if (dataList != null && dataList.size() > 0) {
            for (ProductRatingCumulativeData data : dataList) {
                UserEventType userEventType = UserEventType.getEvent(data.getEventType());
                if (userEventType != null) {
                    /**
                     * SUM counters only positive events
                     */
                    switch (userEventType) {
                        case VIEW:
                        case CLICK:
                        case LIKE:
                        case FAVOURITE:
                        case ADD_TO_CART:
                        case BUY:
                            counter += data.getCounter();
                            break;
                    }
                    result += (userEventType.getScore() * data.getCounter());
                }
            }
            result += (m * c);
            if (counter != 0) {
                result /= (counter + m);
                return result;
            }
        }
        return result;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductInstantRating> getLastInstantRatings(Long productId, Long countryId, int countOfLastResults) {
        return productInstantRatingRepository.getLastInstantRatings(productId, countryId, countOfLastResults);
    }

    @Transactional(readOnly = true)
    @Override
    public Double movingAverage(Long productId, Long countryId, int countOfLastResults) {
        return productInstantRatingRepository.movingAverage(productId, countryId, countOfLastResults);
    }
}
