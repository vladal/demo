package com.lotsof.services.rating.instantRating;

import com.lotsof.entity.rating.ProductInstantRating;

import java.util.List;

/**
 * Created by vlad.
 */
public interface ProductInstantRatingService {
    void create(Long productId, Long countryId, Double instantRating);

    Double calculateProductInstantRating(Long productId, Long countryId, final Double m, final Double c);

    List<ProductInstantRating> getLastInstantRatings(Long productId, Long countryId, int countOfLastResults);

    Double movingAverage(Long productId, Long countryId, int countOfLastResults);
}
