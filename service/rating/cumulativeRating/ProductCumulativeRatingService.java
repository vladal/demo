package com.lotsof.services.rating.cumulativeRating;

import java.util.List;

/**
 * Created by vlad.
 */
public interface ProductCumulativeRatingService {
    int update(Long productId, Double rating);

    Double getRating(Long productId);

    Double getMaxRating();

    Double getAverageRating();

    Long getProductIdWithMaxRating();

    List<Long> getProductIdListSortedByRating(int firstResult, int maxResults);
}
