package com.lotsof.services.rating.cumulativeRating;

import com.lotsof.repository.rating.productCumulativeRating.ProductCumulativeRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by vlad.
 */
@Service
public class ProductCumulativeRatingServiceImpl implements ProductCumulativeRatingService {
    @Autowired
    private ProductCumulativeRatingRepository productCumulativeRatingRepository;

    @Transactional
    @Override
    public int update(Long productId, Double rating) {
        int count = productCumulativeRatingRepository.update(productId, rating);
        if (count == 0) {
            productCumulativeRatingRepository.create(productId, rating);
            return 1;
        }
        return count;
    }

    @Transactional(readOnly = true)
    @Override
    public Double getRating(Long productId) {
        return productCumulativeRatingRepository.getRating(productId);
    }

    @Transactional(readOnly = true)
    @Override
    public Double getMaxRating() {
        return productCumulativeRatingRepository.getMaxRating();
    }

    @Transactional(readOnly = true)
    @Override
    public Double getAverageRating() {
        return productCumulativeRatingRepository.getAverageRating();
    }

    @Transactional(readOnly = true)
    @Override
    public Long getProductIdWithMaxRating() {
        return productCumulativeRatingRepository.getProductIdWithMaxRating();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getProductIdListSortedByRating(int firstResult, int maxResults) {
        return productCumulativeRatingRepository.getProductIdListSortedByRating(firstResult, maxResults);
    }

}
