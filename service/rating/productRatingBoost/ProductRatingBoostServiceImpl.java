package com.lotsof.services.rating.productRatingBoost;

import com.lotsof.entity.rating.ProductRatingBoost;
import com.lotsof.repository.rating.productRatingBoost.ProductRatingBoostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by vlad.
 */
@Service
public class ProductRatingBoostServiceImpl implements ProductRatingBoostService {

    @Autowired
    private ProductRatingBoostRepository productRatingBoostRepository;

    @Transactional
    @Override
    public int create(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId, Calendar boostDate) {
        ProductRatingBoost productRatingBoost = getByProductId(productId, boostCountryId, userId);
        if (productRatingBoost != null) {
            if (productRatingBoost.getBoostToDate().before(boostDate)) {
                remove(productRatingBoost);
                return productRatingBoostRepository.create(productId, boostFactor, boostValue, boostCountryId, userId, boostDate);
            } else {
                return 0;
            }
        } else {
            return productRatingBoostRepository.create(productId, boostFactor, boostValue, boostCountryId, userId, boostDate);
        }
    }

    @Transactional
    @Override
    public int create(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId) {
        Calendar boostDate = Calendar.getInstance();
        return create(productId, boostFactor, boostValue, boostCountryId, userId, boostDate);
    }

    @Transactional
    @Override
    public void create(ProductRatingBoost productRatingBoost) {
        Long productId = productRatingBoost.getProductId();
        Double boostFactor = productRatingBoost.getBoostFactor();
        Double boostValue = productRatingBoost.getBoostValue();
        Long boostCountryId = productRatingBoost.getBoostCountryId();
        Long userId = productRatingBoost.getUserId();
        create(productId, boostFactor, boostValue, boostCountryId, userId);
    }

    @Transactional(readOnly = true)
    @Override
    public ProductRatingBoost read(Long id) {
        return productRatingBoostRepository.read(id);
    }

    @Transactional(readOnly = true)
    @Override
    public ProductRatingBoost getByProductId(Long productId, Long boostCountryId, Long userId) {
        return productRatingBoostRepository.getByProductId(productId, boostCountryId, userId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductRatingBoost> getListByProductId(Long productId, Long boostCountryId) {
        return productRatingBoostRepository.getListByProductId(productId, boostCountryId);
    }

    @Transactional(readOnly = true)
    @Override
    public Map<Long, List<ProductRatingBoost>> getBoostMapByProductId(Long productId, List<Long> boostCountryIdList) {
        return productRatingBoostRepository.getBoostMapByProductId(productId, boostCountryIdList);
    }

//    @Transactional
//    @Override
//    public int update(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Calendar boostDate) {
//        int rows = productRatingBoostRepository.update(productId, boostFactor, boostValue, boostCountryId, boostDate);
//        if (rows == 0) {
//            rows = productRatingBoostRepository.create(productId, boostFactor, boostValue, boostCountryId, boostDate);
//        }
//        return rows;
//    }
//
//    @Transactional
//    @Override
//    public int update(Long productId, Double boostFactor, Double boostValue, Long boostCountryId) {
//        int rows = productRatingBoostRepository.update(productId, boostFactor, boostValue, boostCountryId);
//        if (rows == 0) {
//            rows = productRatingBoostRepository.create(productId, boostFactor, boostValue, boostCountryId);
//        }
//        return rows;
//    }
//
//    @Transactional
//    @Override
//    public int update(ProductRatingBoost productRatingBoost) {
//        int rows = productRatingBoostRepository.update(productRatingBoost);
//        if (rows == 0) {
//            productRatingBoostRepository.create(productRatingBoost);
//            if (productRatingBoost.getId() != null && productRatingBoost.getId() > 0) {
//                rows = 1;
//            }
//        }
//        return rows;
//    }

    @Transactional
    @Override
    public void remove(ProductRatingBoost productRatingBoost) {
        productRatingBoostRepository.remove(productRatingBoost);
    }

    @Transactional
    @Override
    public void remove(Long id) {
        productRatingBoostRepository.remove(id);
    }

    @Transactional
    @Override
    public void remove(Long productId, Long boostCountryId, Long userId) {
        productRatingBoostRepository.remove(productId, boostCountryId, userId);
    }

    @Transactional
    @Override
    public void removeProduct(Long productId, Long boostCountryId, Long userId) {
        productRatingBoostRepository.removeProductRatingBoost(productId, boostCountryId, userId);
    }

    @Transactional(readOnly = true)
    @Override
    public Double getBoostFactor(Long productId, Long boostCountryId, Long userId) {
        return productRatingBoostRepository.getBoostFactor(productId, boostCountryId, userId);
    }

    @Transactional(readOnly = true)
    @Override
    public Double getSumBoost(Long productId, Long boostCountryId) {
        return productRatingBoostRepository.getSumBoostValue(productId, boostCountryId);
    }

    @Transactional(readOnly = true)
    @Override
    public Double getBoostValue(Long productId, Long boostCountryId, Long userId) {
        Double boostValue = productRatingBoostRepository.getRatingBoostValue(productId, boostCountryId, userId);
        if (boostValue == null) {
            boostValue = 0.0;
        }
        return boostValue;
    }

    @Transactional(readOnly = true)
    @Override
    public Calendar getBoostDate(Long productId, Long boostCountryId, Long userId) {
        return productRatingBoostRepository.getBoostDate(productId, boostCountryId, userId);
    }

    @Transactional(readOnly = true)
    @Override
    public Calendar getBoostToDate(Long productId, Long boostCountryId, Long userId) {
        return productRatingBoostRepository.getBoostToDate(productId, boostCountryId, userId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getProductIdList(int firstResult, int maxResults) {
        return productRatingBoostRepository.getProductIdList(firstResult, maxResults);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getActiveProductIdList(int firstResult, int maxResults) {
        return productRatingBoostRepository.getActiveProductIdList(firstResult, maxResults);
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductRatingBoost> getProductRatingBoostList(int firstResult, int maxResults) {
        return productRatingBoostRepository.getProductRatingBoostList(firstResult, maxResults);
    }

    @Transactional
    @Override
    public void resetProductRatingBoost(Long productId) {
        productRatingBoostRepository.resetProductRatingBoost(productId);
    }

//    @Transactional(readOnly = true)
//    @Override
//    public List<ProductRatingBoost> getActiveProductRatingBoostList(int firstResult, int maxResults) {
//        return productRatingBoostRepository.getActiveProductRatingBoostList(firstResult, maxResults);
//    }
}
