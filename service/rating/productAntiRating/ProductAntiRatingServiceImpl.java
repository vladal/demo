package com.lotsof.services.rating.productAntiRating;

import com.lotsof.entity.rating.ProductAntiRating;
import com.lotsof.repository.rating.productAntiRating.ProductAntiRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

/**
 * Created by vlad on 3/16/16.
 */
@Service
public class ProductAntiRatingServiceImpl implements ProductAntiRatingService {
    @Autowired
    private ProductAntiRatingRepository productAntiRatingRepository;

    @Transactional
    @Override
    public void create(Long productId, Double antiRatingValue, Long userId, Calendar antiRatingDate) {
        if (productId != null && antiRatingValue != null && userId != null && antiRatingDate != null && antiRatingValue > 0) {
            productAntiRatingRepository.create(productId, antiRatingValue, userId, antiRatingDate);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public ProductAntiRating read(Long id) {
        if (id != null) {
            return productAntiRatingRepository.read(id);
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public void delete(Long id) {
        if (id != null) {
            productAntiRatingRepository.delete(id);
        }
    }

    @Transactional
    @Override
    public void deleteProductAntiRating(Long productId) {
        if (productId != null) {
            productAntiRatingRepository.deleteProductAntiRating(productId);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Double getAntiRatingValue(Long id) {
        Double result = 0.0;
        if (id != null) {
            Double antiRatingValue = productAntiRatingRepository.getAntiRatingValue(id);
            if (antiRatingValue != null) {
                result = antiRatingValue;
            }
        }
        return result;
    }

    @Transactional(readOnly = true)
    @Override
    public Double getSumOfAntiRatingValues(Long productId) {
        Double result = 0.0;
        if (productId != null) {
            Double sumOfAntiRatingValues = productAntiRatingRepository.getSumOfAntiRatingValues(productId);
            if (sumOfAntiRatingValues != null) {
                result = sumOfAntiRatingValues;
            }
        }
        return result;
    }
}
