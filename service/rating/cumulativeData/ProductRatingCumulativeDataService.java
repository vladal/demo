package com.lotsof.services.rating.cumulativeData;

import com.lotsof.common.rating.UserEventType;
import com.lotsof.entity.rating.ProductRatingCumulativeData;

import java.util.List;

/**
 * Created by vlad.
 */
public interface ProductRatingCumulativeDataService {
    void create(Long productId, UserEventType eventType, Long countryId);

    int update(Long productId, UserEventType eventType, Long countryId);

    Long getCounterValue(Long productId, UserEventType eventType, Long countryId);

    Long getCounterValue(Long productId, UserEventType eventType);

    List<Long> getProductIds();

    List<Long> getCountries(Long productId);

    List<Long> getAllCountries();

    List<ProductRatingCumulativeData> getListProductRatingCumulativeData(Long productId, Long countryId);

    List<Long> getProductIdsWithMaxSells(List<Long> productIds, UserEventType eventType, int maxCount);
}
