package com.lotsof.services.rating.cumulativeData;

import com.lotsof.common.rating.UserEventType;
import com.lotsof.entity.rating.ProductRatingCumulativeData;
import com.lotsof.repository.rating.cumulativeData.ProductRatingCumulativeDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by vlad.
 */
@Service
public class ProductRatingCumulativeDataServiceImpl implements ProductRatingCumulativeDataService {

    @Autowired
    private ProductRatingCumulativeDataRepository productRatingCumulativeDataRepository;

    @Transactional
    @Override
    public void create(Long productId, UserEventType eventType, Long countryId) {
        productRatingCumulativeDataRepository.create(productId, eventType, countryId);
    }

    @Transactional
    @Override
    public int update(Long productId, UserEventType eventType, Long countryId) {
        return productRatingCumulativeDataRepository.update(productId, eventType, countryId);
    }

    @Transactional(readOnly = true)
    @Override
    public Long getCounterValue(Long productId, UserEventType eventType, Long countryId) {
        return productRatingCumulativeDataRepository.getCounterValue(productId, eventType, countryId);
    }

    @Transactional(readOnly = true)
    @Override
    public Long getCounterValue(Long productId, UserEventType eventType) {
        return productRatingCumulativeDataRepository.getCounterValue(productId, eventType);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getProductIds() {
        return productRatingCumulativeDataRepository.getProductIds();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getCountries(Long productId) {
        return productRatingCumulativeDataRepository.getCountries(productId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getAllCountries() {
        return productRatingCumulativeDataRepository.getAllCountries();
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductRatingCumulativeData> getListProductRatingCumulativeData(Long productId, Long countryId) {
        return productRatingCumulativeDataRepository.getListProductRatingCumulativeData(productId, countryId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Long> getProductIdsWithMaxSells(List<Long> productIds, UserEventType eventType, int maxCount) {
        return productRatingCumulativeDataRepository.getProductIdsWithMaxSells(productIds, eventType, maxCount);
    }

}
