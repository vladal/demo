package com.lotsof.services.rating.calculate;

import com.lotsof.common.rating.UserEventType;
import com.lotsof.entity.ShopStatus;
import com.lotsof.entity.rating.ProductRatingBoost;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by vlad.
 */
public interface RatingService {
    void collectProductRawData(Long userId, Long countryId, UserEventType eventType, Long productId, Long shopId);

    void cumulateProductRawData(Long productId, UserEventType eventType, Long countryId);

    void createProductInstantRating(Long productId, Long countryId, final Double m, final Double c);

    int updateProductRating(Long productId, Long countryId);

    int updateProductCumulativeRating(Long productId, final int marketPlaceCountriesAmount, final int productCountriesAmount);

    int updateProductCumulativeRating(Long productId, Double rating);

    void createProductRatingBoost(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId, Calendar boostDate);

    void createProductRatingBoost(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId);

    void createProductRatingBoost(ProductRatingBoost productRatingBoost);

    void removeProductRatingBoost(Long productId, Long boostCountryId, Long userId);

//    int updateProductRatingBoost(Long productId, Double boostFactor, Double boostValue, Long boostCountryId);
//
//    int updateProductRatingBoost(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Calendar boostDate);

    Double getRating(Long productId);

    Double getRating(Long productId, Long countryId);

    Double getFullRating(Long productId, Long countryId);

    Map<Long, Double> getRating(Long productId, List<Long> countryIdList);

    Double getMaxRating();

    Double getMaxRating(Long countryId);

    Double getMaxRatingByCountry(Long countryId);

    Double getRatingWithBoost(Long productId, Long countryId);

//    Map<Long, Double> getRatingWithBoost(Long productId, List<Long> countryIdList);

    Map<Long, Double> getRatingWithBoost(Double productRatingOnMarketplace,
                                         Map<Long, Double> productRatingMap,
                                         Map<Long, List<ProductRatingBoost>> productBoostMap);

    Double getRatingWithCore(Long productId, Long countryId);

    Map<Long, Double> getRatingWithCore(Double productRatingOnMarketPlace,
                                        ShopStatus shopStatus,
                                        Map<Long, Double> productRatingMap,
                                        Map<Long, List<ProductRatingBoost>> productBoostMap);

    Double getBoostFactor(Long productId, Long boostCountryId, Long userId);

    Double getBoostValue(Long productId, Long boostCountryId, Long userId);

    Map<Long, List<ProductRatingBoost>> getProductBoostMap(Long productId, List<Long> boostCountryIdList);

    Double getSumBoost(Long productId, Long boostCountryId);

    Double getSumBoost(List<ProductRatingBoost> productRatingBoostList);

    Long getProductIdWithMaxRatingByCountry(Long countryId);

    Long getProductIdWithMaxRating();

    List<Long> getProductIdListSortedByRating(Long countryId, int firstResult, int maxResults);

    Long getCounterValue(Long productId, UserEventType eventType);

    Long getCounterValue(Long productId, UserEventType eventType, Long countryId);

    Double getAverageRating();

    Double getAverageRating(Long countryId);

    Double getInitialRating();

    Map<Long, Double> getCountryRatingWithBoostMap(Long productId, List<Long> countryIdlist);

    Map<Long, Double> getCountryRatingWithCoreMap(Long productId, List<Long> countryIdlist);

    void invalidateProductRatingBoost(ProductRatingBoost productRatingBoost);

    void invalidateRatingBoost();

    void processRating();

    void resetProductRating(Long productId, Long countryId, Long userId);

    Double getSumAntiRating(Long productId);
}
