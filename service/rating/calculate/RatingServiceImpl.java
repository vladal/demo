package com.lotsof.services.rating.calculate;

import com.lotsof.common.CustomRejectedExecutionHandler;
import com.lotsof.common.DaemonThreadFactory;
import com.lotsof.common.LocaleEnum;
import com.lotsof.common.elastic.LotsOfIndex;
import com.lotsof.common.rating.UserEventType;
import com.lotsof.config.SystemConfig;
import com.lotsof.country.CountryResolver;
import com.lotsof.entity.ShopStatus;
import com.lotsof.entity.rating.ProductRatingBoost;
import com.lotsof.services.CountryService;
import com.lotsof.services.product.ProductService;
import com.lotsof.services.rating.cumulativeData.ProductRatingCumulativeDataService;
import com.lotsof.services.rating.cumulativeRating.ProductCumulativeRatingService;
import com.lotsof.services.rating.instantRating.ProductInstantRatingService;
import com.lotsof.services.rating.productAntiRating.ProductAntiRatingService;
import com.lotsof.services.rating.productRating.ProductRatingService;
import com.lotsof.services.rating.productRatingBoost.ProductRatingBoostService;
import com.lotsof.services.rating.rawData.ProductRatingRawDataService;
import com.lotsof.services.search.ElasticSearchIndexingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by vlad.
 */
@Service
@EnableScheduling
public class RatingServiceImpl implements RatingService {

    private final static Double INITIAL_RATING = 0.0;
    private final static Double M = 1.0;
    private final static Double CORE = 100.0;

    private static final Logger LOGGER = LoggerFactory.getLogger(RatingServiceImpl.class);

    @Autowired
    private ProductInstantRatingService productInstantRatingService;

    @Autowired
    private ProductRatingCumulativeDataService productRatingCumulativeDataService;

    @Autowired
    private ProductRatingRawDataService productRatingRawDataService;

    @Autowired
    private ProductRatingService productRatingService;

    @Autowired
    private ProductRatingBoostService productRatingBoostService;

    @Autowired
    private ProductCumulativeRatingService productCumulativeRatingService;

    @Autowired
    private ProductAntiRatingService productAntiRatingService;

    @Autowired
    private ElasticSearchIndexingService indexingService;

    @Autowired
    private CountryResolver countryResolver;

    @Autowired
    private CountryService countryService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SystemConfig systemConfig;

    @Autowired
    private DaemonThreadFactory daemonThreadFactory;

    @Autowired
    private CustomRejectedExecutionHandler rejectedExecutionHandler;

    private static final int NUMBER_OF_THREADS = 4;

    private BlockingQueue<Runnable> runnableQueue = new ArrayBlockingQueue<>(NUMBER_OF_THREADS);

    private ExecutorService ratingExecutor;

    @PostConstruct
    public void initExecutor() {
        ratingExecutor = new ThreadPoolExecutor(0, NUMBER_OF_THREADS,
                10L, TimeUnit.SECONDS, runnableQueue, daemonThreadFactory, rejectedExecutionHandler);
    }

    @Transactional
    @Override
    public void collectProductRawData(Long userId, Long countryId, UserEventType eventType, Long productId, Long shopId) {
        if (eventType != null && countryId != null && productId != null && shopId != null) {
            productRatingRawDataService.create(userId, countryId, eventType, productId, shopId);
            UserEventType oppositeEvent = null;
            switch (eventType) {
                case LIKE:
                    oppositeEvent = UserEventType.UNDO_LIKE;
                    break;
                case UNDO_LIKE:
                    oppositeEvent = UserEventType.LIKE;
                    break;
                case FAVOURITE:
                    oppositeEvent = UserEventType.UNDO_FAVOURITE;
                    break;
                case UNDO_FAVOURITE:
                    oppositeEvent = UserEventType.FAVOURITE;
                    break;
                case ADD_TO_CART:
                    oppositeEvent = UserEventType.UNDO_ADD_TO_CART;
                    break;
                case UNDO_ADD_TO_CART:
                    oppositeEvent = UserEventType.ADD_TO_CART;
                    break;
                case BUY:
                    oppositeEvent = UserEventType.RETURN;
                    break;
                case RETURN:
                    oppositeEvent = UserEventType.BUY;
                    break;
            }

            if (oppositeEvent != null) {
                if (eventType.getEvent().contains("undo") || eventType.getEvent().contains("return")) {
                    Long eventCounter = productRatingCumulativeDataService.getCounterValue(productId, eventType, countryId);
                    Long oppositeEventCounter = productRatingCumulativeDataService.getCounterValue(productId, oppositeEvent, countryId);
                    if (oppositeEventCounter != null) {
                        if (eventCounter == null || oppositeEventCounter > eventCounter) {
                            cumulateProductRawData(productId, eventType, countryId);
                        }
                    }
                } else {
                    cumulateProductRawData(productId, eventType, countryId);
                }
            } else {
                cumulateProductRawData(productId, eventType, countryId);
            }
        }
    }

    @Transactional
    @Override
    public void cumulateProductRawData(Long productId, UserEventType eventType, Long countryId) {
        if (productRatingCumulativeDataService.update(productId, eventType, countryId) == 0) {
            productRatingCumulativeDataService.create(productId, eventType, countryId);
        }
    }

    @Override
    public void createProductInstantRating(Long productId, Long countryId, final Double m, final Double c) {
        Double instantRating = productInstantRatingService.calculateProductInstantRating(productId, countryId, m, c);
        productInstantRatingService.create(productId, countryId, instantRating);
    }

    @Override
    public int updateProductRating(Long productId, Long countryId) {
        Double rating = productRatingService.calculateProductRatingByCountry(productId, countryId);
        return productRatingService.update(productId, countryId, rating);
    }

    @Override
    public int updateProductCumulativeRating(Long productId, final int marketPlaceCountriesAmount, final int productCountriesAmount) {
        Double rating = productRatingService.calculateProductRating(productId);
        if (marketPlaceCountriesAmount > productCountriesAmount && marketPlaceCountriesAmount > 0) {
            rating *= (1.0 * productCountriesAmount) / marketPlaceCountriesAmount;
            rating += (1.0 * (marketPlaceCountriesAmount - productCountriesAmount)) / marketPlaceCountriesAmount;
        }
        return productCumulativeRatingService.update(productId, rating);
    }

    @Override
    public int updateProductCumulativeRating(Long productId, Double rating) {
        return productCumulativeRatingService.update(productId, rating);
    }

    @Override
    public void createProductRatingBoost(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId, Calendar boostDate) {
        productRatingBoostService.create(productId, boostFactor, boostValue, boostCountryId, userId, boostDate);
        indexingService.updateProductRating(productId, LotsOfIndex.LOTSOF_ELASTICSEARCH_INDEX_NAME, LocaleEnum.RU);
    }

    @Override
    public void createProductRatingBoost(Long productId, Double boostFactor, Double boostValue, Long boostCountryId, Long userId) {
        productRatingBoostService.create(productId, boostFactor, boostValue, boostCountryId, userId);
        indexingService.updateProductRating(productId, LotsOfIndex.LOTSOF_ELASTICSEARCH_INDEX_NAME, LocaleEnum.RU);
    }

    @Override
    public void createProductRatingBoost(ProductRatingBoost productRatingBoost) {
        productRatingBoostService.create(productRatingBoost);
        Long productId = productRatingBoost.getProductId();
        Long boostCountryId = productRatingBoost.getBoostCountryId();
        indexingService.updateProductRating(productId, LotsOfIndex.LOTSOF_ELASTICSEARCH_INDEX_NAME, LocaleEnum.RU);
    }

    @Override
    public void removeProductRatingBoost(Long productId, Long boostCountryId, Long userId) {
        productRatingBoostService.remove(productId, boostCountryId, userId);
        indexingService.updateProductRating(productId, LotsOfIndex.LOTSOF_ELASTICSEARCH_INDEX_NAME, LocaleEnum.RU);
    }

    @Override
    public Double getRating(Long productId) {
        Double rating = productCumulativeRatingService.getRating(productId);
        Double sumOfAntiRatingValues = productAntiRatingService.getSumOfAntiRatingValues(productId);
        if (rating == null || rating < 0.0) {
            rating = INITIAL_RATING;
        }
        if (rating.compareTo(sumOfAntiRatingValues) >= 0) {
            rating -= sumOfAntiRatingValues;
        } else {
            rating = INITIAL_RATING;
        }
        return rating;
    }

    @Override
    public Double getRating(Long productId, Long countryId) {
        Double rating = productRatingService.getProductRatingByCountry(productId, countryId);
        Double sumOfAntiRatingValues = productAntiRatingService.getSumOfAntiRatingValues(productId);
        if (rating == null || rating < 0.0) {
            rating = INITIAL_RATING;
        }
        if (rating.compareTo(sumOfAntiRatingValues) >= 0) {
            rating -= sumOfAntiRatingValues;
        } else {
            rating = INITIAL_RATING;
        }
        return rating;
    }

    @Override
    public Double getFullRating(Long productId, Long countryId) {
        return getRating(countryId, productId) + getRating(productId);
    }

    @Override
    public Map<Long, Double> getRating(Long productId, List<Long> countryIdList) {
        if (countryIdList == null || countryIdList.isEmpty()) {
            countryIdList = countryService.getAllCountriesId();
        }
        Map<Long, Double> productRatingsByCountryList = productRatingService.getProductRatingsByCountryList(productId, countryIdList);
        Double sumOfAntiRatingValues = productAntiRatingService.getSumOfAntiRatingValues(productId);
        for (Map.Entry<Long, Double> countryProductRating : productRatingsByCountryList.entrySet()) {
            Double rating = countryProductRating.getValue();
            if (rating == null || rating < 0.0) {
                rating = INITIAL_RATING;
            }
            if (rating.compareTo(sumOfAntiRatingValues) >= 0) {
                rating -= sumOfAntiRatingValues;
            } else {
                rating = INITIAL_RATING;
            }
            countryProductRating.setValue(rating);
        }
        return productRatingsByCountryList;
    }

    @Override
    public Double getMaxRating() {
        Double maxRating = productCumulativeRatingService.getMaxRating();
        if (maxRating == null || maxRating <= 0.0) {
            maxRating = INITIAL_RATING;
        }
        return maxRating;
    }

    @Override
    public Double getMaxRating(Long countryId) {
        Double maxRating = productRatingService.getMaxProductRatingByCountry(countryId);
        if (maxRating == null || maxRating <= 0.0) {
            maxRating = INITIAL_RATING;
        }
        return maxRating;
    }

    @Override
    public Double getMaxRatingByCountry(Long countryId) {
        Double maxRating = productRatingService.getMaxProductRatingByCountry(countryId);
        if (maxRating == null || maxRating <= 0.0) {
            maxRating = INITIAL_RATING;
        }
        return maxRating;
    }

    /**
     * Add to product rating boost and average product rating on marketplace
     *
     * @param productId
     * @param countryId
     * @return
     */
    @Override
    public Double getRatingWithBoost(Long productId, Long countryId) {
        Double rating = getFullRating(productId, countryId);
        rating += getSumBoost(productId, countryId);
        return rating;
    }

    @Override
    public Map<Long, Double> getRatingWithBoost(Double productRatingOnMarketplace,
                                                Map<Long, Double> productRatingMap,
                                                Map<Long, List<ProductRatingBoost>> productBoostMap) {
        Map<Long, Double> result = null;
        if (productRatingOnMarketplace != null && productRatingMap != null && productBoostMap != null) {
            result = new HashMap<>(productRatingMap);
            for (Long boostCountryId : productBoostMap.keySet()) {
                Double tempRating = result.get(boostCountryId);
                tempRating += getSumBoost(productBoostMap.get(boostCountryId));
                tempRating += productRatingOnMarketplace;
                result.replace(boostCountryId, tempRating);
            }
        }
        return result;
    }

    @Override
    public Double getRatingWithCore(Long productId, Long countryId) {
        Double rating = getFullRating(productId, countryId);
        rating += getSumBoost(productId, countryId) * 0.01;
        if (ShopStatus.CORE.equals(productService.getShopStatus(productId))) {
            rating += CORE;
        }
        return rating;
    }

    @Override
    public Map<Long, Double> getRatingWithCore(Double productRatingOnMarketPlace,
                                               ShopStatus shopStatus,
                                               Map<Long, Double> productRatingMap,
                                               Map<Long, List<ProductRatingBoost>> productBoostMap) {
        Map<Long, Double> result = null;
        if (productRatingMap != null && productBoostMap != null) {
            result = new HashMap<>(productRatingMap);
            for (Long countryId : productRatingMap.keySet()) {
                Double tempRating = result.get(countryId);
                List<ProductRatingBoost> productRatingBoostList = productBoostMap.get(countryId);
                if (productRatingBoostList != null) {
                    tempRating += getSumBoost(productRatingBoostList) * 0.01;
                }
                if (ShopStatus.CORE.equals(shopStatus)) {
                    tempRating += CORE;
                }
                tempRating += productRatingOnMarketPlace;
                result.replace(countryId, tempRating);
            }
        }
        return result;
    }

    @Override
    public Double getBoostFactor(Long productId, Long boostCountryId, Long userId) {
        Double boostFactor = productRatingBoostService.getBoostFactor(productId, boostCountryId, userId);
        if (boostFactor == null) {
            boostFactor = 0.0;
        }
        return boostFactor;
    }

    @Override
    public Double getBoostValue(Long productId, Long boostCountryId, Long userId) {
        Double boostValue = productRatingBoostService.getBoostValue(productId, boostCountryId, userId);
        if (boostValue == null) {
            boostValue = 0.0;
        }
        return boostValue;
    }

    @Override
    public Map<Long, List<ProductRatingBoost>> getProductBoostMap(Long productId, List<Long> boostCountryIdList) {
        return productRatingBoostService.getBoostMapByProductId(productId, boostCountryIdList);
    }

    @Override
    public Double getSumBoost(Long productId, Long boostCountryId) {
        List<ProductRatingBoost> productRatingBoostList = productRatingBoostService.getListByProductId(productId, boostCountryId);
        return getSumBoost(productRatingBoostList);
    }

    @Override
    public Double getSumBoost(List<ProductRatingBoost> productRatingBoostList) {
        Double boostSum = 0.0;
        if (productRatingBoostList != null && !productRatingBoostList.isEmpty()) {
            for (ProductRatingBoost productRatingBoost : productRatingBoostList) {
                if (productRatingBoost.getBoostValue() != null && productRatingBoost.getBoostToDate() != null) {
                    boostSum += calculateCurrentBoost(productRatingBoost);
                }
            }
        }
        return boostSum;
    }

    private double calculateCurrentBoost(ProductRatingBoost productRatingBoost) {
        double currentBoostValue = 0.0;
        long currentDate = System.currentTimeMillis();
        long boostStartDate = productRatingBoost.getBoostDate().getTimeInMillis();
        long boostEndDate = productRatingBoost.getBoostToDate().getTimeInMillis();
        double boostValue = productRatingBoost.getBoostValue();
        double normalize = 1.0 * (currentDate - boostStartDate) / (boostEndDate - boostStartDate);
        if (currentDate <= boostEndDate) {
            currentBoostValue = boostValue * (1 - normalize);
        }
        return currentBoostValue;
    }

    @Override
    public Long getProductIdWithMaxRatingByCountry(Long countryId) {
        return productRatingService.getProductIdWithMaxRatingByCountry(countryId);
    }

    @Override
    public Long getProductIdWithMaxRating() {
        return productCumulativeRatingService.getProductIdWithMaxRating();
    }

    @Override
    public List<Long> getProductIdListSortedByRating(Long countryId, int firstResult, int maxResults) {
        List<Long> productIdList = productRatingService.getProductIdListSortedByRating(countryId, firstResult, maxResults);
        if (productIdList == null || productIdList.isEmpty()) {
            productIdList = productCumulativeRatingService.getProductIdListSortedByRating(firstResult, maxResults);
        }
        return productIdList;
    }

    @Override
    public Long getCounterValue(Long productId, UserEventType eventType) {
        Long result = productRatingCumulativeDataService.getCounterValue(productId, eventType);
        if (result == null) {
            result = 0L;
        }
        return result;
    }

    @Override
    public Long getCounterValue(Long productId, UserEventType eventType, Long countryId) {
        Long result = productRatingCumulativeDataService.getCounterValue(productId, eventType, countryId);
        if (result == null) {
            result = 0L;
        }
        return result;
    }


    @Override
    public Double getAverageRating() {
        Double averageRating = productCumulativeRatingService.getAverageRating();
        if (averageRating <= 0) {
            return INITIAL_RATING;
        } else {
            return averageRating;
        }
    }

    @Override
    public Double getAverageRating(Long countryId) {
        Double averageRating = productRatingService.getAverageRating(countryId);
        if (averageRating <= 0) {
            return INITIAL_RATING;
        } else {
            return averageRating;
        }
    }

    @Override
    public Double getInitialRating() {
        return INITIAL_RATING;
    }

    @Override
    public Map<Long, Double> getCountryRatingWithBoostMap(Long productId, List<Long> countryIdlist) {
        Map<Long, Double> result = null;
        if (productId != null && countryIdlist != null && !countryIdlist.isEmpty()) {
            result = new HashMap<>(countryIdlist.size());
            for (Long countryId : countryIdlist) {
                result.put(countryId, getRatingWithBoost(productId, countryId));
            }
        }
        return result;
    }

    @Override
    public Map<Long, Double> getCountryRatingWithCoreMap(Long productId, List<Long> countryIdlist) {
        Map<Long, Double> result = null;
        if (productId != null && countryIdlist != null && !countryIdlist.isEmpty()) {
            result = new HashMap<>(countryIdlist.size());
            for (Long countryId : countryIdlist) {
                result.put(countryId, getRatingWithCore(productId, countryId));
            }
        }
        return result;
    }

    @Override
    public void invalidateProductRatingBoost(ProductRatingBoost productRatingBoost) {
        productRatingBoostService.remove(productRatingBoost);
        indexingService.updateProductRating(productRatingBoost.getProductId(), LotsOfIndex.LOTSOF_ELASTICSEARCH_INDEX_NAME, LocaleEnum.RU);
    }

    @Scheduled(cron = "0 10 */2 * * *")
    @Override
    public void invalidateRatingBoost() {
        if ("LIVE".equals(systemConfig.getRatingsProfile().toUpperCase())) {
            LOGGER.info("@SCHEDULED invalidate rating start");
            try {
                int firstResult = 0;
                int maxResults = 10;
                List<ProductRatingBoost> productRatingBoostList =
                        productRatingBoostService.getProductRatingBoostList(firstResult, maxResults);
                Calendar currentDate = Calendar.getInstance();
                while (productRatingBoostList != null && !productRatingBoostList.isEmpty()) {
                    for (ProductRatingBoost productRatingBoost : productRatingBoostList) {
                        if (currentDate.after(productRatingBoost.getBoostToDate())) {
                            invalidateProductRatingBoost(productRatingBoost);
                        }
                    }
                    firstResult += maxResults;
                    productRatingBoostList =
                            productRatingBoostService.getProductRatingBoostList(firstResult, maxResults);
                }
                processRating();
            } catch (Exception e) {
                LOGGER.error("@SCHEDULED invalidate rating error", e);
            }
            LOGGER.info("@SCHEDULED invalidate rating finish");
        }
    }

    @Override
    public void processRating() {
        long zeroTime = 0L;
        if ("LIVE".equals(systemConfig.getRatingsProfile().toUpperCase())) {
            LOGGER.info("Start create ratings");
            zeroTime = System.currentTimeMillis();
            List<Long> productIdList = productRatingCumulativeDataService.getProductIds();
            if (productIdList != null && !productIdList.isEmpty()) {
                Double c = productCumulativeRatingService.getAverageRating();
                if (c <= 0) {
                    c = INITIAL_RATING;
                }
                final Double averageRating = c;
                CountDownLatch countDownLatch = new CountDownLatch(productIdList.size());
                final int marketplaceCountriesAmount = countryService.getCountriesAmout();
                for (Long productId : productIdList) {
                    ratingExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                List<Long> countryIdList = productRatingCumulativeDataService.getCountries(productId);
                                if (countryIdList != null && !countryIdList.isEmpty()) {
                                    int productCountriesAmount = countryIdList.size();
                                    for (Long countryId : countryIdList) {
                                        createProductInstantRating(productId, countryId, M, averageRating);
                                        updateProductRating(productId, countryId);
                                    }
                                    updateProductCumulativeRating(productId, marketplaceCountriesAmount, productCountriesAmount);
                                }
                            } catch (Exception e) {
                                LOGGER.error("Exception when processing ratings. " + e.getLocalizedMessage());
                            } finally {
                                countDownLatch.countDown();
                            }
                        }
                    });
                }
                try {
                    countDownLatch.await(50L, TimeUnit.MINUTES);
                } catch (InterruptedException e) {
                    LOGGER.warn("Time out when processRating method executes");
                }
                indexingService.updateProductsRating(LotsOfIndex.LOTSOF_ELASTICSEARCH_INDEX_NAME, LocaleEnum.RU);
            }
            LOGGER.info("Time to create ratings: " + ((System.currentTimeMillis() - zeroTime) / 1000.0) + "sec");
        }
    }

    @Override
    public void resetProductRating(Long productId, Long countryId, Long userId) {
        if (productId != null && userId != null) {
            Double productRating = getFullRating(productId, countryId);
            productAntiRatingService.create(productId, productRating, userId, Calendar.getInstance());
            productRatingBoostService.resetProductRatingBoost(productId);
            indexingService.updateProductRating(productId, LotsOfIndex.LOTSOF_ELASTICSEARCH_INDEX_NAME, LocaleEnum.RU);
        }
    }

    @Override
    public Double getSumAntiRating(Long productId) {
        return productAntiRatingService.getSumOfAntiRatingValues(productId);
    }
}
