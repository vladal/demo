package com.lotsof.services.rating.productRating;

import java.util.List;
import java.util.Map;

/**
 * Created by vlad.
 */
public interface ProductRatingService {
    int update(Long productId, Long countryId, Double rating);

    Double getProductRatingByCountry(Long productId, Long countryId);

    Map<Long, Double> getProductRatingsByCountryList(Long productId, List<Long> countryIdList);

    Double getMaxProductRatingByCountry(Long countryId);

    Long getProductIdWithMaxRatingByCountry(Long countryId);

    Double getAverageRating(Long countryId);

    List<Long> getProductIdListSortedByRating(Long countryId);

    List<Long> getProductIdListSortedByRating(Long countryId, int firstResult, int maxResults);

    Double calculateProductRatingByCountry(Long productId, Long countryId);

    Double calculateProductRating(Long productId);
}