package com.lotsof.services.rating.productRating;

import com.lotsof.repository.rating.productRating.ProductRatingRepository;
import com.lotsof.services.rating.instantRating.ProductInstantRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by vlad.
 */
@Service
public class ProductRatingServiceImpl implements ProductRatingService {

    private final static int FREQ_OF_INSTANT_RATING_CALC = 1;
    private final static int SIXTY_DAYS = 60 * (24 / FREQ_OF_INSTANT_RATING_CALC);
    private final static int TEN_DAYS = 10 * (24 / FREQ_OF_INSTANT_RATING_CALC);

    @Autowired
    private ProductInstantRatingService productInstantRatingService;

    @Autowired
    private ProductRatingRepository productRatingRepository;

    @Transactional
    @Override
    public int update(Long productId, Long countryId, Double rating) {
        int count = productRatingRepository.update(productId, countryId, rating);
        if (count == 0) {
            create(productId, countryId, rating);
            return 1;
        } else {
            return count;
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Double getProductRatingByCountry(Long productId, Long countryId) {
        return productRatingRepository.getProductRatingByCountry(productId, countryId);
    }

    @Transactional(readOnly = true)
    @Override
    public Map<Long, Double> getProductRatingsByCountryList(Long productId, List<Long> countryIdList) {
        return productRatingRepository.getProductRatingsByCountryList(productId, countryIdList);
    }

    @Transactional(readOnly = true)
    @Override
    public Double getMaxProductRatingByCountry(Long countryId) {
        return productRatingRepository.getMaxProductRatingByCountry(countryId);
    }

    @Transactional(readOnly = true)
    @Override
    public Long getProductIdWithMaxRatingByCountry(Long countryId) {
        return productRatingRepository.getProductIdWithMaxRatingByCountry(countryId);
    }

    @Transactional(readOnly = true)
    @Override
    public Double getAverageRating(Long countryId) {
        return productRatingRepository.getAverageRating(countryId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getProductIdListSortedByRating(Long countryId) {
        return productRatingRepository.getProductIdListSortedByRating(countryId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Long> getProductIdListSortedByRating(Long countryId, int firstResult, int maxResults) {
        return productRatingRepository.getProductIdListSortedByRating(countryId, firstResult, maxResults);
    }

    @Override
    public Double calculateProductRatingByCountry(Long productId, Long countryId) {
        Double longMA = productInstantRatingService.movingAverage(productId, countryId, SIXTY_DAYS);
        Double shortMA = productInstantRatingService.movingAverage(productId, countryId, TEN_DAYS);
        return Math.sqrt(longMA * shortMA);
    }

    @Transactional(readOnly = true)
    @Override
    public Double calculateProductRating(Long productId) {
        return productRatingRepository.getProductRating(productId);
    }


    @Transactional
    private void create(Long productId, Long countryId, Double rating) {
        productRatingRepository.create(productId, countryId, rating);
    }

}
